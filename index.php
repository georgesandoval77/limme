<?php
$filecss = substr(basename(__FILE__), 0, -3);    // jala nombre del archivo para pedirlo como css
include("encabezado.php");
?>
    <script>    // Funciones para mostrar y ocultar el video, modificando también el botón de selección.
        function trae_video() {
            var elemento = document.createElement("video");    // llena contenedor del video
            //elemento.className = "elemento_class"
            elemento.setAttribute("controls","");
            var fuente = document.createElement("source");
            fuente.setAttribute("src", "video/limmecov.mp4");
            fuente.setAttribute("type", "video/mp4");
            elemento.append(fuente);
            document.getElementById('contenedor-video').append(elemento);  

            document.getElementById('contenedor-boton').innerHTML=" ";  // vacía contenedor del boton
            var boton = document.createElement("button");   // prepara mensaje del contenedor del botón
            boton.setAttribute("class","elvideo");
            boton.setAttribute("type","submit");
            boton.setAttribute("onclick","quita_video();");
            boton.innerHTML = "Ocultar video"    
            document.getElementById('contenedor-boton').append(boton);
            if (window.matchMedia("(min-width: 768px)").matches) {
                document.getElementById('contenedor-video').style.padding = "25px 25px 50px"; //padding con el video, arriba de 768px de pantalla
            } else {document.getElementById('contenedor-video').style.padding = "5px";} //padding con el video, abajo de 768px de pantalla
        }
        function quita_video() {
            document.getElementById('contenedor-video').innerHTML=" ";  // vacía contenedor del video
            document.getElementById('contenedor-boton').innerHTML=" ";  // vacía contenedor del boton
            var p1 = document.createElement("p");           // prepara mensaje del contenedor del botón
            var p2 = document.createElement("p");
            p1.innerHTML = "Mira el ";
            p2.innerHTML = " del Limme en tiempos de Covid";    
            var boton = document.createElement("button");
            boton.setAttribute("class","elvideo");
            boton.setAttribute("type","submit");
            boton.setAttribute("onclick","trae_video();");
            boton.innerHTML = "video"    
            document.getElementById('contenedor-boton').append(p1,boton,p2); 
            document.getElementById('contenedor-video').style.padding = "5px";   // padding sin el video.
        }
    </script>

    <h1>Bienvenidos al Limme</h1> 
    <div id="contenedor-video" class="contenedor-video"></div>
    <div id="contenedor-boton" class="contenedor-boton">
        <?php
        echo "<p>Mira el </p>";
        echo "<button class='elvideo' type='submit' onclick='trae_video();'>video</button>";
        echo "<p> del limme en tiempos de Covid</p>"
        // echo "<button class='elvideo' type='submit' onclick='trae_video();'>Mira el video del Limme en tiempos de Covid</button>";    
        ?>
    </div>
    <br>

    <div class="foto"><img src="img/aula_b.jpg" alt="Aula B" /></div>
    
        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-6 d-flex align-content-between p-0">  
                    <p>El Laboratorio de Informática Musical y Música Electroacústica o
                        Limme es el área donde los alumnos inscritos en la FaM entran en
                        contacto con la tecnología musical.<br>Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons
                        equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-6 d-flex align-content-between p-0">
                    <p>Consta de un estudio de grabación con dos salas de mezcla y dos
                        Aulas equipadas con equipo de sonido, pantalla, teclados midi y
                        computadoras.<br>Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali
                        qua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor
                        is nisi ut aliquip ex ea commodo consequat. Duis aute irure sed do
                        eiusmod tempor incididunt ut labore.</p>
                </div>
            </div>
        </section>

        <div class="foto"><img src="img/estudio_b.jpg" alt="Estudio B" /></div>
        
        <section class="container">
            <div class="row d-flex justify-content-center w-100">
                <div class="col-12 col-md-6 d-flex align-content-between p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                        do eiusmod <a href="materias.php">Materias</a> cididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, <a href="profesores.php">Profesores</a> ation ullamco laboris
                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                        in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur <a href="horarios.php">Horarios</a> te cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id
                        est laborum. Sed ut perspiciatis. </p>
                </div>
                <div class="col-12 col-md-6 d-flex align-content-between p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                        do eiusmod te <a href="actividades.php">Actividades</a> labore et dolore magna aliqua. Ut
                        enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        nisi ut aliquip ex ea <br>Commodo consequat. Duis aute irure dolor
                        in repre <a href="material-didactico.php">Material Didáctico</a> et cillum dolore eu fugiat nulla
                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt
                        in culpa qui officia. </p>
                </div>
            </div>
        </section>

        <div class="foto"><img src="img/aula_a.jpg" alt="Aula A" /></div>

<?php
include("footer.php");
?>