<?php
$filecss = substr(basename(__FILE__), 0, -3);
include("encabezado.php");
?>
    <h1>Horarios</h1>
        <section class="horario">
            <div class="grid-container">
                <div class="titulo1"><h2>Horario de clases del LIMME</h2></div>
                <div class="titulo2"><h2>Semestre 20-1</h2></div>
                <div class="hlunes"><img class="foto" src="img/HLunes.png" /></div>
                <div class="hmartes"><img class="foto" src="img/HMartes.png" /></div>
                <div class="hmiercoles"><img class="foto" src="img/HMiercoles.png" /></div>
                <div class="hjueves"><img class="foto" src="img/HJueves.png" /></div>
                <div class="hviernes"><img class="foto" src="img/HViernes.png" /></div>
                <div class="hsabado"><img class="foto" src="img/HSabado.png" /></div>
            </div>
        </section>

<?php
include("footer.php");
?>