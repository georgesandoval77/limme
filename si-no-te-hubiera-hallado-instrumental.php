<?php
$filecss = "act_paginas.";    
include("encabezado.php");
?>

    <h1>Si no te hubiera hallado (instrumental)</h1><br><br><br> 
    <div id="fecha"><p id="fecha">25 SEPTIEMBRE, 2020 DE TOLOQUERO</p></div>
    <div id="fecha"><p id="fecha">Composición: Roberto Salazar</p></div>
    <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F833946598&auto_play=false&hide_related=false&visual=false&show_comments=true&show_user=true&show_reposts=false&color=ff5500"></iframe>
    <div clase="seccion">
        <h2>Descripción general</h2>
        <p>Originalmente fue una pieza pensada como una canción, sin embargo, debido a diferentes circunstancias, el proyecto terminó siendo completamente instrumental. Es una pieza con un carácter de tango/milonga para piano, guitarra y concertina anglo, que rescata los colores del tango de A. Piazzolla. Por otra parte es importante mencionar que no es una producción meramente de música académica con la perspectiva de que “lo que se logra en la interpretación es lo que queda grabado” pues echo mano de kilos y kilos de edición.</p>
    </div>
    <div clase="seccion">
        <h2>Descripción técnica</h2>
        <p>Para la grabación de la guitarra y la concertina se utilizó un micrófono de condensador, específicamente el RODE NT1-A. Para la concertina se grabó a una distancia de 1m aproximadamente ya que el instrumento es muy resonante, para la guitarra fue a una distancia de 40cm. Todo esto fue grabado a media noche para no tener interrupciones de ruido en la calle.
<br><br>
Por otra parte, la parte del piano fue completamente MIDI debido a que el piano con el que cuento está desafinado, esto sirvió también para tener una especie de claqueta.
<br><br>
En cuanto a la edición las partes de la concertina y la guitarra tuvieron una gran serie de cortes debido a que el dominio sobre dichos instrumentos no es muy bueno, llegando a esconder quizá los errores que se tenían. Cada parte tiene un reverb y un ligero compresor.
<br><br>
La parte de la concertina está duplicada, pero a la pista doble se le bajó 15 cents de afinación, esto fue para emular el timbre de uno de sus instrumentos hermanos, el acordeón. El acordeón cuenta con al menos dos lengüetas por nota, dándole su característico timbre, la concertina sólo tiene una lengüeta por lo que para resolver esta cuestión bastaba con hacerlo de manera digital. cada una de estas pistas fue paneada a lados diferentes.
<br><br>
Finalmente se añadieron ligeras percusiones para darle movimiento, primeramente una fusión entre el golpe a las cuerdas de la guitarra, chasquidos y unos claps MIDI que entran en tiempos 2 y 4. También unas claves acentuando el bajo de los tiempos 4 y 1 de algunos compases.</p>
    </div>
    <div id="foto"><img src="imga/Si no te hubiera hallado instrumental-1.png"></div>
    <div id="fecha"><p id="fecha">Imagen 1. Sesión en donde los clips azules son la concertina y la guitarra con un montón de corte</p></div>
    <div id="foto"><img src="imga/Si no te hubiera hallado instrumental-2.png"></div>
    <div id="fecha"><p id="fecha">Imagen 2. Sesión ya terminada con los clips de la concertina y la guitarra más unificados</p></div>
    
    <div clase="seccion">
        <h2>Dificultades presentadas</h2>
        <p>Las dificultades presentadas en primera instancia fue conseguir al cantante para tener su parte, consecuencia de que el proyecto se transformara a un instrumental. Por otra parte hubo un problema en cuanto a interpretación de la pieza como ya fue mencionado anteriormente. En la parte técnica prácticamente no hubo mucho problema pues eché mano de un par de cosas que ya tenía conocimiento previo, es decir, a nivel de DAW, me sentí como pez en el agua.  </p>
    </div>
    <div clase="seccion">
        <h2>Conclusión general</h2>
        <p>Fue un proyecto de lo más interesante para mí a tal grado de tener ganas, incluso, de grabar un par de piezas de Piazzolla, con esta misma dotación, y subir a redes un pequeño EP de tangos compuestos, arreglados y producidos por mí.</p>
    </div>
 
<?php
include("footer.php");
?>