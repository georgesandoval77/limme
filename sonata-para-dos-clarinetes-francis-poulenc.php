<?php
$filecss = "act_paginas.";    
include("encabezado.php");
?>

    <h1>Sonata para dos clarinetes- Francis Poulenc (1918)</h1><br><br><br> 
    <div id="fecha"><p id="fecha">4 OCTUBRE, 2020 DE AORTEGAEB95</p></div>
    <div id="foto"><img src="imga/Sonata para dos clarinetes- Francis Poulenc-1.png"></div>
    
    <div clase="seccion">
        <h2>Descripción general</h2>
        <p> Proyecto improvisado a causa de la cuarentena, donde abordo una obra para clarinetes del compositor francés Francis Poulenc. Esta obra implicó para mí involucrarme en todo el proceso, desde transcripción hasta producción de audio y video.<br>

La grabación es del primer movimiento de la sonata, el cual presenta un grado de dificultad elevado; tiene forma ternaria e introduce polimétrica en ambas voces, cambios de tempo, carácter y articulaciones.

</p>
    </div>
    <div clase="seccion">
        <h2>Descripción técnica</h2>
        <p>La grabación se realizó en un cuarto pequeño con una grabadora ZOOM H6, en formato WAV/ 48khz/ 24 bits. <br>

Comencé con varias tomas de prueba con el micrófono en XY y en MS, decidiéndome al final por el último, el cual coloqué a 50 cm del instrumento.<br>

La H6 tiene la función de hacer una grabación de respaldo con -12dB en caso de que durante la toma se sature el audio, lo cual fue de mucha ayuda durante el procesamiento en Ardour.</p>
    </div>
    <div id="foto"><img src="imga/Sonata para dos clarinetes- Francis Poulenc-2.jpeg"></div>
    <div id="foto"><img src="imga/Sonata para dos clarinetes- Francis Poulenc-3.jpeg"></div>
    <div clase="seccion">
        <p> El primer día grabé únicamente la parte del saxofón alto (clarinete en la) para usarla como base. Después de decidir la toma ideal, grabé el soprano siguiendo la pista. Debido a la velocidad del movimiento (presto), conté en voz alta y respiré en las entradas con mayor fuerza a la normal, para que al momento de grabar el saxofón soprano me fuera más claro el momento exacto para tocar.<br>

Para el día siguiente ya había estudiado la pista del alto. Dado que aún no sabía usar la H6 desde Ardour como interfaz, la reproduje desde iTunes para guiarme.<br>

Después de escuchar y depurar las tomas, importé los 2 tracks finales con sus respectivos back ups a Ardour 6; de ahí decidí usar el back up de la voz soprano y el normal del alto, para mayor control. Agregué 2 buses de audio, con compresor y reverberación después del fader; recorté el inicio y final del movimiento; moví unos milisegundos para mayor exactitud en ciertas entradas; agregué fade in/out, y para finalizar combiné las pistas</p>
    </div>
    <div id="foto"><img src="imga/Sonata para dos clarinetes- Francis Poulenc-4.png"></div>
    <div clase="seccion">
        <p>Utilicé automatización de mute en una parte específica donde se cayó algo dentro del cuarto de grabación e hizo mucho ruido, así como fader sobre la pista del alto para bajarla un par de decibeles- sobre todo la parte B donde el motivo que se repite a lo largo alcanza notas sobreagudas- con tal de no tapar la melodía principal que lleva el soprano.</p>
    </div>
    <div id="foto"><img src="imga/Sonata para dos clarinetes- Francis Poulenc-5.png"></div>
    <div clase="seccion">
        <p>Después de exportar el audio, importé la pista y videos a iMovie, donde emparejé todo; la claqueta del inicio me ayudó bastante a lograrlo. Le di mute al audio de los videos y sólo agregué un cuadro negro al final como fade out de la toma.<br>

Exporté tres versiones finales del audio, ya que al escuchar el producto final comencé a pensar que podría mejorar mis automatizaciones para que se escuchara más orgánico.</p>
    </div>
    <div id="foto"><img src="imga/Sonata para dos clarinetes- Francis Poulenc-6.png"></div>
    <div clase="seccion">
        <h2>Dificultades presentadas</h2>
        <p>El primer reto fue conseguir la partitura. Toqué esta obra hace un año con un compañero para un festival, así que me benefició no abordarla por primera vez para avanzar más rápido en este proceso; sin embargo, en esa ocasión yo toqué la voz soprano, así que nunca tuve en mis manos la transcripción del alto. Intenté pedirla a mi compañero, quien se encuentra totalmente desconectado del mundo digital, por lo que al no conseguirla comencé a trabajar en Sibelius. No pensé que fuera difícil, ya que no tenía que modificar registros, sólo transportar; sin embargo tardé dos horas en completar 12 compases. Me rendí. No fue hasta que descargué una app en mi iPad (Notion) que me sentí más capaz de terminar la tarea.<br>

En cuanto a la grabación, en total hice 41 tomas de audio y video, por lo que tuve que hacer una bitácora para no mezclar un video con un audio equivocado. Las primeras 20 fueron completa basura; en mis pruebas con los micrófonos y distancia entre el instrumento y la grabadora decidí mal y grabé la obra completa, haciendo menos eficientes las siguientes tomas.<br><br>

Por otra parte, la música y ruido de los vecinos- así como de los otros habitantes de mi casa- dificultan mucho encontrar momentos de silencio.  En muchas ocasiones, estos ruidos- que afortunadamente no se filtraron a la grabación- impidieron que me concentrara debidamente.<br>

Finalmente, cuadrar exactamente cada audio con cada video me resultó difícil, ya que se trataba de mi primera vez usando una aplicación de video, por lo que investigué comandos básicos y vi varios tutoriales para utilizarla. Durante la exportación noté que se desfasaban audio y video, así que regresé varias veces a identificar exactamente dónde se encontraba el problema. Al final me di cuenta de que debí grabar en horizontal para no quedarme con esa línea negra entre mis dos videos, pero ya no pude hacer nada al respecto.

</p>
    </div>

    <div clase="seccion">
        <h2>Conclusión</h2>
        <p> Este proyecto me ayudó a involucrarme más en áreas que no manejo bien, así como a ser enteramente responsable de todo este proceso: desde conseguir la obra, estudiar, y definir mis cambios de tempo hasta grabar y procesar la información. Fue igualmente útil recordar que lo que no se graba bien desde el inicio no se puede arreglar en producción.<br>

Lo sufrí y lo disfruté; el resultado no me desagradó en lo absoluto. Me gustaría continuar y grabar los otros dos movimientos para no sentir que dejo incompleto este proyecto.<br>

 Al principio tenía pensado grabar un trío de piano, chelo y saxofón, lo cual me emocionaba bastante; me entristece que no haya sido posible debido a la cuarentena. Los ejercicios que hicimos en Ardour a lo largo del semestre, así como esta práctica final, me dejaron con muchas dudas que tuve que aprender a resolver por mi cuenta, ya que no quería molestar a Emi o a Jorge con cada cosa que me frenaba. Creo que todos los errores que cometí me han ayudado a aprender, por lo que en un futuro espero poder llevar a cabo el ciclo entero de producción más rápida y eficientemente.<br><br>

 Esta práctica me sirvió para decidirme por grabación como mi forma de titulación. Tengo un proyecto en mente que me gustaría desarrollar, así que producción III y IV están dentro de mis planes.<br>

 Encuentro inspirador el trabajo que se lleva a cabo en el LIMME, así como el ambiente que se percibe dentro de él, por lo que me gustaría mantenerme involucrada en sus actividades a futuro.<br>

 Gracias a ambos por tener la paciencia y apertura que llevan a cada clase</p>
    </div>


 
<?php
include("footer.php");
?>