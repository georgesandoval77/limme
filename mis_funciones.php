<?php

function es_entero($valor, $min, $max) {
    if( ctype_digit($valor) && $valor >= $min && $valor <= $max) {
        return true;
    }
    return false;
}

function vacio($valor) {
    if ($valor === 0 || $valor == '0') {return false;}     
    if (empty($valor)) {return true;} else {return false;}
}

// conexión a la base de datos
    DEFINE ('DB_USER', 'root');
    DEFINE ('DB_PASSWORD', '');
    DEFINE ('DB_HOST', 'localhost');
    DEFINE ('DB_NAME', 'limme');

    $dbc = mysqli_connect (DB_HOST, DB_USER, DB_PASSWORD, DB_NAME); //conexión
    if ($dbc) {
        mysqli_set_charset($dbc, 'utf8');           // set de caracteres
    }

?>