<?php
$filecss = substr(basename(__FILE__), 0, -3);
include("encabezado.php");
?>
    <h1>Materias</h1>
        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>INFORMATICA MUSICAL I</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. </p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>INFORMATICA MUSICAL II</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. D.</p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>MUSICA Y MEDIOS<br> AUDIOVISUALES</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. </p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>PRODUCCION MUSICAL<br> Y REGISTRO SONORO I</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. D.</p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>PRODUCCION MUSICAL<br> Y REGISTRO SONORO II</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. </p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>TECNICAS DE MICROFONIA ESTEREOFONICA</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. D.</p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>MUSICA ELECTROACUSTICA I</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. </p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>MUSICA ELECTROACUSTICA II</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. D.</p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>PRACTICAS DE SALA<br>LABORATORIO DE GRABACION PROFESIONAL</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. </p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>PERSPECTIVAS CRITICAS DE TECNOLOGIA MUSICAL</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. D.</p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>FUNDAMENTOS DE COMPUTO MUSICAL</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. </p>
                </div>
            </div>
        </section>

        <section class="container">
            <div class="row justify-content-center w-100">
                <div class="col-12 col-md-4 p-0">  
                    <h5>MUSICA, ARTE Y TECNOLOGIA</h5>
                </div>
                <div class="col-12 col-md-4 p-0">  
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation <br>Lorem ipsum dolor sit amet, consectetur 
                        adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                        quis nostr ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. Duis aute irure. </p>
                </div>
                <div class="col-12 col-md-4 p-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                        magna ali qua. Ut enim ad minim veniam, quis nostrud exercitation ipsum dolor sit amet, consectetur adipisicing 
                        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostr
                        ud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons equat. D.</p>
                </div>
            </div>
        </section>

<?php
include("footer.php");
?>