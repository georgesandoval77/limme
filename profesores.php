<?php
$filecss = substr(basename(__FILE__), 0, -3);
include("encabezado.php");
?>
    <h1>Profesores</h1>
        <section class="container">
            <div class="row inicio justify-content-center w-100">
                <div class="col-12 col-md-6 d-flex align-content-between p-0">  
                    <img class="logo-limme" src="img/p_pablo.jpg" alt="Pablo Silva" />
                </div>
                <div class="col-12 col-md-6 d-flex align-content-between p-0">
                    <div class="row inicio2 justify-content-center w-100">
                        <div class="col-12" >
                            <h5>Pablo Silva Treviño</h5>
                        </div>
                        <div class="col-12">
                            <p>Asignaturas: Informática musical, producción musical, laboratorio de artes digitales</p>
                        </div>
                        <div class="col-12">
                            <p>Licenciado en piano por la Escuela Nacional de Música, Master of Fine Arts in Compositionpor el California 
                                Institute of the Arts.<br>Fundador del Laboratorio de Informática Musical y Música Electroacústica (LIMME), 
                                del que fue coordinador de 1995 a 2001, y de nuevo a partir de 2006. Formó parte del comité para el desarrollo 
                                Posgrado en Música (áreas de tecnología musical y composición) en la Escuela Nacional de Música.<br>Ha impartido 
                                cursos sobre música electroacústica en el Centro Nacional de las Artes (Cenart) y otros lugares de México, 
                                y ha sido jurado en concursos internacionales de música electroacústica.<br>Cuenta con obras para combinaciones 
                                instrumentales tradicionales y música electroacústica, y ha realizado también música original para cine, teatro 
                                y televisión. Éstas han sido programadas en México, Brasil, Colombia, Venezuela, Argentina, Canadá y EUA, así 
                                como en el Foro Internacional de Música Nueva Manuel Enríquez.<br>Organizador de SISMO 04, Bienal Internacional 
                                de Música Electroacústica, UNAM, agosto 2004. Organizador del programa de intercambio internacional MUXIC, 
                                UNAM-Sorbonne Universités, 2018-2019.<br>Sus intereses principales son la composición electroacústica y mixta, 
                                la composición algorítmica y la realización de obras multimedia, así como la programación algorítmica en vivo 
                                (live coding).</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row inicio justify-content-center w-100">
                <div class="col-12 col-md-6 order-md-12 d-flex p-0">  
                    <img class="logo-limme" src="img/p_jorge.png" alt="Jorge David García" />
                </div>
                <div class="col-12 col-md-6 order-md-1 d-flex p-0">
                    <div class="row inicio2 justify-content-center w-100">
                        <div class="col-12" >
                            <h5>Jorge David García Castilla</h5>
                        </div>
                        <div class="col-12">
                            <p>Asignaturas: Informática musical, música electroacústica, producción musical, perspectivas críticas de tecnología musical</p>
                        </div>
                        <div class="col-12">
                            <p>Licenciado en Composición Musical por el Conservatorio de las Rosas, Maestro y Doctor en Musicología por 
                                el Posgrado en Música de la UNAM.<br>En lo referente a su perfil como investigador, el trabajo de Jorge 
                                David García está enfocado en dos principales líneas de investigación: por una parte, la del impacto que 
                                las tecnologías digitales están teniendo en el campo de la creación musical, y por otra parte la del papel 
                                que la escucha tiene en contextos educativos. Estas dos vertientes investigativas han dado lugar a diversos 
                                proyectos de creación, investigación y gestión cultural. Su marcado interés por la relación entre música y 
                                tecnología ha llevado a Jorge David García a colaborar en diversos espacios dedicados a la investigación, el 
                                desarrollo tecnológico y la creación musical.<br>En este rubro, merece una mención especial su participación 
                                como docente y coordinador del programa especializado sobre tecnología musical con software libre, desarrollado 
                                en la UNAM para la plataforma Coursera, así como su adscripción a la Red de Estudios sobre Sonido y Escucha, 
                                organización independiente conformada por investigadores de diversos campos disciplinares, dedicada a realizar 
                                actividades académicas (seminarios, congresos, publicaciones colectivas) relacionadas con el sonido y la 
                                escucha.<br>En lo referente a su perfil compositivo, Jorge David García mantiene una constante actividad como 
                                compositor, habiendo presentado obras en diversas ciudades de México y el extranjero. Aunado a la composición 
                                de música de concierto, ha trabajado como musicalizador y/o sonidista de distintos proyectos de teatro, danza, 
                                cine y video, y ha participado en distintos colectivos de improvisación libre.<br>Actualmente labora como profesor 
                                de tiempo completo de la Facultad de Música de la UNAM, donde desarrolla actividades de docencia, tutoría, gestión 
                                e investigación a nivel licenciatura, maestría y doctorado. Dentro de sus actividades académicas al interior de dicha 
                                institución, destaca la coordinación del ciclo de conciertos “Resonancias”, dentro del que se han organizado alrededor 
                                de treinta eventos en el transcurso de los últimos dos años.<br>Desde 2020, es miembro candidato del Sistema Nacional 
                                de Investigadores.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row  inicio justify-content-center w-100">
                <div class="col-12 col-md-6 d-flex align-content-between p-0">  
                    <img class="logo-limme" src="img/p_patricio.png" alt="Patricio Calatayud" />
                </div>
                <div class="col-12 col-md-6 d-flex align-content-between p-0">
                    <div class="row inicio2 justify-content-center w-100">
                        <div class="col-12" >
                            <h5>Patricio Calatayud</h5>
                        </div>
                        <div class="col-12">
                            <p>Asignaturas: Producción musical, informática musical, música electroacústica, laboratorio de diseño sonoro</p>
                        </div>
                        <div class="col-12">
                            <p>Patricio F. Calatayud (ARG-MX) es compositor musical (Licenciatura UNAM, M.H., con una investigación sobre 
                                el concepto de “intención”). Es maestro en tecnología musical (maestría UNAM M.H., con una investigación 
                                en nuevas formas de escritura musical en computadoras.<br>Actualmente interpreta sus obras en solitario y 
                                con el ensamble Lysis. Se ha presentado en museos, galerías y numerosos espacios de experimentación sonora 
                                en Latinoamérica.<br>Es profesor definitivo en la Facultad de Música, en el área del LIMME y también da cursos 
                                en el INBA, CENART y TEC de Monterrey. Su línea de investigación gira en torno a la musicografía dinámica, 
                                cognición musical y matemáticas en música.<br><br>https://soundcloud.com/ochuruss<br>
                                <br>https://www.instagram.com/scores__musica/<br><br>https://issuu.com/patriciocalatayud<br>
                                <br>https://unam.academia.edu/PatricioCalatayud<br><br>https://www.youtube.com/channel/UCaSPa2uI3JMIsUzcqmZRm3Q
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row inicio  justify-content-center w-100">
                <div class="col-12 col-md-6 order-md-12 d-flex align-content-between p-0">  
                    <img class="logo-limme" src="img/p_diego.png" alt="Diego Tinajero" />
                </div>
                <div class="col-12 col-md-6 order-md-1 d-flex align-content-between p-0">
                    <div class="row inicio2 justify-content-center w-100">
                        <div class="col-12" >
                            <h5>Diego Tinajero Islas</h5>
                        </div>
                        <div class="col-12">
                            <p>Asignaturas: Producción musical, laboratorio de grabación profesional.</p>
                        </div>
                        <div class="col-12">
                            <p>Licenciado en producción musical y Maestro en tecnología musical.<br>Actualmente es profesor de la Facultad 
                                de Música de la UNAM, en el área de producción musical. Además, es profesor de innovación tecnológica en la 
                                educación en la Universidad Panamericana, y Docente en SAE Institute México, en la Licenciatura de negocios 
                                de la música.<br>Desarrolló el primer curso masivo en línea “Introducción a la producción musical” del programa 
                                especializado “Tecnología musical con software libre” del Posgrado en música de la UNAM.<br>Actualmente desarrolla 
                                contenido educativo y artístico relacionado con la música en su canal “drmidi” a través de la plataforma Youtube.
                                <br>Ha trabajado como productor y músico en diversos proyectos musicales independientes en la ciudad de México como 
                                el grupo de rock fusión “Takto Sperta”. También fue productor y músico del proyecto de trip hop “Minitori” así como 
                                músico ejecutante en el proyecto de música pop “Micca Mont”.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row inicio  justify-content-center w-100">
                <div class="col-12 col-md-6 d-flex align-content-between p-0">  
                    <img class="logo-limme" src="img/p_esteban.jpg" alt="Esteban Chapela" />
                </div>
                <div class="col-12 col-md-6 d-flex align-content-between p-0">
                    <div class="row inicio2 justify-content-center w-100">
                        <div class="col-12" >
                            <h5>Esteban Palafox</h5>
                        </div>
                        <div class="col-12">
                            <p>Asignaturas: Música electroacústica.</p>
                        </div>
                        <div class="col-12">
                            <p>Estudió composición en el Centro de Investigación y Estudios de la Música (CIEM) en la Ciudad de México 
                                y la Maestría en Tecnología Musical en New York University (NYU) en Nueva York, becado por el Consejo 
                                Nacional de Ciencia y Tecnología (CONACYT).<br>Ha trabajado como compositor y diseñador de audio para 
                                cortometrajes, comerciales de televisión y juegos de video. También ha realizado musicalización de 
                                teatro y creado música electroacústica para danza y videoarte. Es maestro en el la Facultad de Música 
                                de la UNAM, el CIEM, la Academia de Arte de Florencia y el Núcleo Integral de Composición (NICO). Una 
                                parte importante de su labor es la programación en Max, lenguaje gráfico de programación con el que 
                                desarrolla herramientas para la creación musical.<br>En sus proyectos musicales ha explorado la mezcla 
                                de medios electrónicos con géneros de música popular como el rock, el funk y el hip hop. En la actualidad 
                                se dedica principalmente a la composición de música electroacústica, género musical para el cual creó los 
                                espectáculos Audiocinema y Perímetro de Parlantes, en busca de generar experiencias auditivas óptimas 
                                para los escuchas.</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>

<?php
include("footer.php");
?>