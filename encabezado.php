<!DOCTYPE html>

<html lang="es">

<head>
    <title>Limme</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
    crossorigin="anonymous"></script>
    <!-- Bootstrap 4 CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
      integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <link href="css/comun.css" rel="stylesheet" />
      <link href="css/<?= $filecss ?>css" rel="stylesheet" />
      </head>

<body>
    <header>
        <section class="container">
            <div class="row">
                <div class="col-2">
                    <img class="logo-unam" src="img/logo_unam.png" alt="Logo Unam" />
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-12">
                            <h1>Facultad de Música de la UNAM</h1>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-7">
                                    <h2>Laboratorio de Informática Musical<br> y Música Electroacústica</h2>
                                </div>
                                <div class="col-12 col-sm-5">
                                    <img class="logo-limme" src="img/logo_limme.png" alt="Logo Limme" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <img class="logo-fam" src="img/logo_fam.png" alt="Logo Fam" />
                </div>
            </div>
        </section>
    </header>

    <main>
        <nav id="menu" class="navbar navbar-expand-sm navbar-dark" >   
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>  
            <div id="collapsibleNavbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav mx-auto justify-content-around navbar-center">  
                <li class="nav-item"><a class="nav-link text-white" href="index.php">Inicio</a></li>
                <li class="nav-item"><a class="nav-link text-white" href="horarios.php">Horarios</a></li>
                <li class="nav-item"><a class="nav-link text-white" href="actividades.php">Actividades</a></li>
                <li class="nav-item"><a class="nav-link text-white" href="profesores.php">Profesores</a></li>
                <li class="nav-item"><a class="nav-link text-white" href="materias.php">Materias</a></li>
                <li class="nav-item"><a class="nav-link text-white" href="captura.php">Calificar</a></li>
              </ul>

            </div>
        </nav>
        <br>

