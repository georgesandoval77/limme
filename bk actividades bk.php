<?php
$filecss = substr(basename(__FILE__), 0, -3);    // jala nombre del archivo para pedirlo como css
include("encabezado.php");
?>
    <h1>Actividades del Limme</h1> 
    <div id="contenedor-actividades" class="contenedor-actividades"></div>

    <script language="javascript" type="text/javascript">
        var n = 0   // contador, que se usa para ponerselo de id a la foto de cada foto
            var resultado = document.getElementById("contenedor-actividades");
            var xmlhttp;
            xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function (aEvt) {
                console.log(xmlhttp.readyState, xmlhttp.status);
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    resultado.innerHTML = xmlhttp.responseText;
                    let actividades = JSON.parse(xmlhttp.responseText);
                    console.log("algo");
                    // console.log(actividades.result);

                    // llena contenedor-actividades de actividades
                    for (acti of actividades.result) {
                        let img = document.createElement("img");
                        img.setAttribute('src', 'img/acti/' + acti.foto);
                    //  info de la pelicula
                        var info = document.createElement("div");   // para guardar los datos de los 2 infos
                        info.className = "info_class"
                        var info1 = document.createElement("p");
                        var info2 = document.createElement("p");
                        var contenido1 = document.createTextNode(acti.obra);
                        var contenido2 = document.createTextNode(acti.fecha);
                        info1.append(contenido1);
                        info2.append(contenido2);
                        info.append(info1);
                        info.append(info2);
                        var elemento = document.createElement("div");       // guarda imagen e info
                        elemento.className = "elemento_class"
                        img.setAttribute("id", n.toString())        // pone id a la imagen para identificarla en caso de
                        img.addEventListener("click", detalle)      // que se quiera consultar su detalle, dando click en la imagen
                        n = n + 1                               // contador, que se usa para ponerselo de id a la foto de cada peli
                        elemento.append(img);
                        elemento.append(info);
                        document.getElementById('contenedor-actividades').append(elemento);
                    }   
                    actividadesaux = actividades            // copia la info para usarla en detalle
                }
            }
            
            xmlhttp.open("GET", "actividades_query.php", true);
            xmlhttp.send();

        // Función detalle, muestra el detalle de una pelicula seleccionada    
            var detalle = function (e){     
                id = e.srcElement.id;       // srcElement o target, la e funge como evento.
                idn = parseInt(id);
                actividadesaux.results[idn].act_hoja_enlazada;

            };

    // enjecución de la 1a vez, antes de mostrar la página
        // let peliculasaux = ""
        // var quien = "botona";
        // var total_paginas = 0;       
        // enviarGet("actual", quien);
        
    </script>
                        
<?php
include("footer.php");
?>