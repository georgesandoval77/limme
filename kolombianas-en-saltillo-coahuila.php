<?php
$filecss = "act_paginas.";    
include("encabezado.php");
?>

    <h1>Kolombianas en Saltillo Coahuila</h1><br><br><br> 
    <div clase="fecha"><p id="fecha">6 OCTUBRE, 2020 DE AORTEGAEB95</p></div>
    <div clase="foto"><img src="imga/kolombianas-en-saltillo-coahuila-1.png"></div>
    <div clase="seccion">
        <h2>Descripción general</h2>
        <p> Mi propósito fue mantener la línea del proyecto MÚSICAS QUE CAMINAN; es decir, contar una historia de vida y el contexto de donde surge.<br>

En el caso de este “documental sonoro” de la cumbia colombiana en Saltillo, Coahuila, cambió un poco la temática, ya que si bien no grabé paisaje sonoro por causa del confinamiento en el que estábamos , sí realicé una entrevista a profundidad por vía telefónica a un chico que baila colombianas en Saltillo. Lo realicé aprovechando que hice un trabajo para análisis etnomusicológico que aborda el tema del performance.</p>
    </div>
    <div clase="seccion">
        <h2>Descripción técnica</h2>
        <p> Primero realicé una entrevista a profundidad a Javier durante mi estancia en confinamiento en Saltillo. Como no pude verlo personalmente le hice una llamada telefónica y la grabé con una TASCAM DR-100. Utilicé el micrófono omnidireccional de la grabadora.<br>

Posteriormente grabé la narrativa que guía el documental.<br>

Después de importar todo el material recopilado a Ardour (voz, narrativa, pistas de audio) comencé la edición.<br>

Hice cortes en donde consideré apropiado para mantener la continuidad de la historia. Pegué, separé, junté, y mezclé.<br>

Utilicé automatizaciones de volumen, fade in y fade out, y al final la master.</p>
    </div>

    <div clase="foto"><img src="imga/kolombianas-en-saltillo-coahuila-2.png"></div>
    <div clase="foto"><img src="imga/kolombianas-en-saltillo-coahuila-3.png"></div>
    <div clase="seccion">
        <h2>Dificultades presentadas</h2>
        <p>Durante la grabación de la entrevista  me percaté de que el volumen estaba muy bajo, por lo que en la edición la voz se escuchaba con eco. Esto mejoró al utilizar un filtro.<br>

No sabía que para importar un archivo éste tenía que estar en formato WAV, así que tardé un poco en descubrirlo mientras revisaba tutoriales.<br>

No tengo mucha experiencia utilizando Ardour, por lo que tuve que buscar herramientas- tales como tutoriales- para familiarizarme más con él, lo que implicó una mayor inversión de tiempo.</p>
    </div>
    <div clase="seccion">
        <h2>Conclusión</h2>
        <p>Disfruté mucho del proceso de creación durante este proyecto; además, me ayudó a resolver dudas que no había logrado aclarar durante el semestre. Conocí mejor el programa y tuve la oportunidad de involucrarme con cada paso de la producción, desde la idea hasta la parte técnica e incluso la parte creativa, desde la cual creo haber desarrollado una auténtica experimentación.<br>

Escuchar terminado el trabajo es muy satisfactorio cuando te involucras y disfrutas de todo el proceso creativo</p>
    </div>
 
<?php
include("footer.php");
?>