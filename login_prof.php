<?php
$filecss = substr(basename(__FILE__), 0, -3);    // jala nombre del archivo para pedirlo como css
include("encabezado.php");
?>
<?php
ob_start(); // reinicia el buffer porque me daba el error   Cannot modify header information headers already sent by
include("mis_funciones.php");

if (isset($_POST['limpiar']) || !isset($_POST['enviar'])) {
    $password = "";
    $num_trab = "";
}

if (isset($_POST['enviar']) && !empty($_POST['enviar'])) {
    $password = trim($_POST['password']);
    $num_trab = trim($_POST['num_trab']);


    $errores = [];
    //Nombre puede tener letras . ' (espacios) -
    
    if( vacio($num_trab) ) {
        $errores['num_trab']['obligatorio'] = "El num_trab es obligatorio";
    } elseif (strlen($num_trab) != 6) {
        $errores['num_trab'][] = "El num_trab es de 6 caracteres";
    }

    if( vacio($password) ) {
        $errores['password']['obligatorio'] = "Password es obligatorio";
    } elseif (strlen($password) < 8) {
        $errores['password'][] = "El password debe tener al menos 8 caracteres";
    }
}
?>
<!-- <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style>
        form {
            width: 80%;
            margin: auto;
        }
        div {
            width: 80%;
            margin: auto;
            display: flex;
            justify-content: center;
            align-items: center;
        }

    </style>
</head>

<body> -->
    <br><br><br>
    <form action="login_prof.php" method="post" enctype="multipart/form-data">
        <fieldset id="datos-color" class="datos-color">
            <legend>Captura de calificaciones</legend>
            <br>
            <h5>Ingresa tu clave</h5>
            <br>
            <p>
                <label for="num_trab">Número de empleado:</label>
                <input type="text" id="num_trab" name="num_trab" value="<?= $num_trab ?>" />
            </p>
            <span class="text-danger">
                <?php
                    if(isset($errores['num_trab']) && !empty($errores['num_trab'])){
                        foreach($errores['num_trab'] as $tipo => $mensaje) {echo $mensaje;}
                    }
                ?>
            </span>

             <p>
                <label for="password" >Contraseña:</label>
                <input type="text" id="password" name="password" value="<?= $password ?>" />
            </p>
            <span class="text-danger">
                <?php
                    if(isset($errores['password']) && !empty($errores['password'])){
                        foreach($errores['password'] as $tipo => $mensaje) {echo $mensaje;}
                    }
                ?>  
            </span>

        </fieldset>

            <div class="contenedor-botones">
                <button type="submit" class="btn" name="enviar" value="1">Enviar</button>
                <button type="submit" class="btn" name="limpiar" value="1">Limpiar</button>
            </div>
            <br><br><br>
    </form>
    
    <br>

    <?php
        if (!isset($_POST['limpiar']) && empty($errores)) { 


            $query = 'SELECT * FROM profesor WHERE prof_numero_trab=? AND prof_password=?';
            $stmt = mysqli_prepare($dbc, $query);
            mysqli_stmt_bind_param($stmt, 'ss', $num_trab, $password);
            mysqli_stmt_execute($stmt); 
            $result = mysqli_stmt_get_result($stmt);

            if ($result) {
                $numRows = mysqli_affected_rows($dbc);
                // print_r($user);
            

            if ($numRows) {
                $user = mysqli_fetch_array($result, MYSQLI_ASSOC);
                print_r($user);
                echo $password;
                echo $user['prof_password'];
                if ($password == $user['prof_password']) {                    
                    session_start();
                    $_SESSION["autorizado"]= TRUE;
                    // $SESSION['user_id'] = $user['user_id'];
                    $SESSION['nombre'] = $user['prof_nombre'];
                    header("Location: captura.php");
                } else {
                    $mensaje = "Datos son incorrectos";
                }
            
            } else{
                $mensaje = "Datos son incorrectos";
            }
                        
            } else {
                $mensaje = "Datos son incorrectos";
            }             
// Para obtener la cantidad de registros que se afectaron con el INSERT, UPDATE o DELETE
            // $numRows = mysqli_affected_rows($dbc);
            // echo "<br/>Registros afectados: ";
            // echo $numRows;

} ?>

<!-- </body>
</html> -->

<?php
include("footer.php");
?> 