<?php
$filecss = substr(basename(__FILE__), 0, -3);    // jala nombre del archivo para pedirlo como css
include("encabezado.php");
?>

<?php
if (isset($_POST['salir'])) {include("salir.php");}
include("seguridad.php");
//include("encabezado.php");
include("mis_funciones.php"); // también la conexión a la base de datos

$isemestre = "";
$icalificacion = "";


if (!empty($_POST)) {
    $numcta = trim($_POST['numcta']);

    $errores = [];
    if( vacio($numcta) ) {
        $errores['numcta']['obligatorio'] = "Ingresa tu número de cuenta";
    } else {
        if( !es_entero($numcta, 000000001, 999999999)) {
            $errores['numcta']['entero'] = "Este número de cuenta no es un valor válido";
        }
    }
}
if (isset($_POST['limpiar']) || empty($_POST)) {
    $numcta = "";
    $imateria = "";
}
if (!isset($_POST['enviar']) && !isset($_POST['limpiar']) && !empty($_POST)) {
    $imateria = trim($_POST['imateria']);
    $isemestre = trim($_POST['isemestre']);
    $icalificacion = trim($_POST['icalificacion']);
}

?>

<form action="captura.php" method="post" enctype="multipart/form-data">
<br><br><br>
        <fieldset id="datos-color" class="datos-color">
            <legend>Captura de calificaciones</legend>
            <br>
            <h5>Ingresa el número de cuenta del alumno</h5>
            <br>
             <p>
                <label for="clave">Número de cuenta:</label>
                <input type="text" id="numcta" name="numcta" value="<?= $numcta ?>" />
                <button type="submit" class="btn" name="enviar" value="1">Enviar</button>
            </p>
            <span class="text-danger">
                <?php
                    if(isset($errores['numcta']) && !empty($errores['numcta'])){
                        foreach($errores['numcta'] as $tipo => $mensaje) {echo $mensaje;}
                    }
                ?>  
            </span>

<?php
            if (!isset($_POST['limpiar']) && !empty($_POST)) { 
                              
                              // Busca nombre y id del alumno
                                  $query = "SELECT a.alum_nombre as nombrea, a.id_alumno as ida
                                  FROM alumno a
                                  WHERE a.alum_numero_cta = $numcta";
                                  $result = mysqli_query ($dbc, $query);
                                  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                                  $nombre = $row['nombrea'];
                                  $ida = $row['ida'];
                          
                              // Alta de calificación
                                  if (isset($_POST['alta_mat']) && empty($errores))   {
                                      $query = "INSERT INTO calificacion (id_alumno, id_materia, cal_valor, cal_semestre)
                                                VALUES ($ida, $imateria, '$icalificacion', '$isemestre')";
                                      $result = mysqli_query ($dbc, $query);
                                      if ($result) { 
                                          $numRows = mysqli_affected_rows($dbc);}
                                  }          
                              // Cambio de calificación
                                  if (isset($_POST['cambio_mat']) && empty($errores))   {
                                      $query = "UPDATE calificacion SET cal_valor = '$icalificacion', cal_semestre = '$isemestre'
                                                WHERE id_alumno = $ida and id_materia = $imateria";
                                      $result = mysqli_query ($dbc, $query);
                                      print_r ($result);
                                      if ($result) { 
                                          $numRows = mysqli_affected_rows($dbc);}
                                  }                         
                              // Baja de calificación       
                                  if (isset($_POST['baja_mat']) && empty($errores))   {
                                      $query = "DELETE FROM calificacion WHERE id_alumno = $ida and id_materia = $imateria";
                                      $result = mysqli_query ($dbc, $query);
                                      if ($result) { 
                                          $numRows = mysqli_affected_rows($dbc);}
                                  }
                          
                          
                                      // Busca todas las materias en que ha estado inscrito el alumno
                                          $query = "SELECT c.cal_semestre as semestre, m.mat_nombre as materia, c.cal_valor as calif
                                          FROM calificacion c 
                                          INNER JOIN materia m ON (c.id_materia = m.id_materia)
                                          INNER JOIN alumno a ON (c.id_alumno = a.id_alumno)
                                          WHERE a.alum_numero_cta = $numcta
                                          ORDER BY semestre";       
                          
                                          $result = mysqli_query ($dbc, $query);  // resultado de la consulta
                                          if ($result) { 
                                              $numRows = mysqli_affected_rows($dbc);
                          
                                              if ($numRows) {
                                                      echo "<p>Alumno: $nombre</p>";      
                                                    //   echo "<div class='tabla'>";
                                                      echo "<table class='tabla'>";
                                                      echo "<tr>";          
                                                      echo "<th>SEMESTRE</th>";
                                                      echo "<th>MATERIA</th>";
                                                      echo "<th>CALIFICACION</th>";
                                                      echo "</tr>";
                                                  while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                                                      echo "<tr>";          
                                                      echo "<td>$row[semestre]</td>";
                                                      echo "<td>$row[materia]</td>";
                                                      echo "<td>$row[calif]</td>";
                                                      echo "</tr>";
                                                  }
                                                      echo "</table>";
                                                      echo "<br><br>";
                                                    //   echo "</div>";
                                              }
                          
                                          }
                               }
?>



            <?php
                if (!isset($_POST['limpiar']) && !empty($_POST) && empty($errores['numcta'])) { ?>
                    <p>
                        <label for="clave">Materia:</label>
                                                                        <!-- class="form-control" -->
                        <select name="imateria">          
                            <?php
                                $query = "SELECT id_materia, mat_nombre FROM materia";
                                $result = mysqli_query ($dbc, $query);                             
                                   while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
                                    <option value="<?php echo $row['id_materia']?>"> <?php echo $row['mat_nombre']?> </option>
                                    <?php 
                                    }
                                    ?>
                        </select><br>

                        <label for="isemestre">Semestre:</label>
                        <input type="text" id="isemestre" name="isemestre" value="<?= $isemestre ?>" /><br>
                        <label for="icalificacion">Calificación:</label>
                        <input type="text" id="icalificacion" name="icalificacion" value="<?= $icalificacion ?>" />
                    </p>
            <?php } ?>   
        </fieldset>

            <div class="contenedor-botones">
                <p>
                <button type="submit" class="btn" name="alta_mat" value="1">Alta</button>
                <button type="submit" class="btn" name="cambio_mat" value="1">Cambio</button>
                <button type="submit" class="btn" name="baja_mat" value="1">Baja</button>
                <button type="submit" class="btn" name="limpiar" value="1">Limpiar</button>
                <br><br>
                <button type="submit" class="btn" name="salir" value="1">Salir</button>
                </p>
            </div>
    </form>
    
    <br><br><br>

<?php
include("footer.php");
?>

