<?php
$filecss = "act_paginas.";    
include("encabezado.php");
?>

    <h1>Cadenza del concierto para violonchelo de Federico Ibarra</h1><br><br><br> 
    <div id="fecha"><p id="fecha">6 OCTUBRE, 2020 DE AORTEGAEB95</p></div>
    <div id="foto"><img src="imga/Cadenza del concierto para violonchelo de Federico Ibarra.jpeg"></div>
    <!-- <div id="video"><video controls><source src="https://youtu.be/9WDhNVDGwPI" type="video/mp4" /></video></div> -->
    <iframe width="560" height="315" src="https://www.youtube.com/embed/9WDhNVDGwPI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <div clase="seccion">
        <h2>Descripción general</h2>
        <p>Grabación audiovisual de la cadenza del concierto para violonchelo y orquesta del compositor mexicano Federico Ibarra.</p>
    </div>
    <div clase="seccion">
        <h2>Descripción técnica</h2>
        <p> Al grabar utilicé mi celular y una aplicación para guardar audio descomprimido ( RODE Rec LE). Realicé este proceso en mi habitación, colocando el celular a una distancia aproximada de un metro. El resultado final es el montaje de catorce partes distintas que revisé minuciosamente antes de editar y mexclar en Ardour 5.<br>

Una vez que el audio estuvo listo, me dediqué a la grabación del video desde la sala de mi casa. Al tener ya una idea clara de lo que buscaba, sólo hizo falta una aplicación de video vintage (Dazz Cam) y utilizar mi celular como videocámara, siempre desde el atril: razón por la cual no cuento con fotos de este proceso. Posteriormente edité el video en iMovie.</p>
    </div>
    <div clase="seccion">
        <h2>Dificultades presentadas</h2>
        <p> Entre los distintos problemas que encontré puedo mencionar el no contar con un espacio aislado del ruido exterior, mi poca familiaridad con Ardour, tener que volver a grabar cuando un ángulo no era óptimo, y por supuesto emparejar el sonido con la imagen.</p>
    </div>
    <div clase="seccion">
        <h2>Conclusión</h2>
        <p> Si bien al comenzar este proyecto no estaba totalmente seguro de lo que hacía, siento que gradualmente mis dudas desaparecieron y logré adquirir una perspectiva creativa. Me siento motivado por el resultado y me gustaría llevar a cabo proyectos similares en el futuro. Tal vez lo más valioso que obtuve durante este proceso fue la experiencia de realizarlo tan solo con los medios a mi alcance y sin contar con un estudio o gente para ayudarme.</p>
    </div>
 
<?php
include("footer.php");
?>